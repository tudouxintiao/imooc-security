package com.imooc.security.core.validate.code;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import com.imooc.security.core.properties.KaptchaProperties;
import com.imooc.security.core.properties.SecurityProperties;
import com.imooc.security.core.validate.code.image.ImageCodeGenerator;
import com.imooc.security.core.validate.code.sms.SmsCodeGenerator;
import com.imooc.security.core.validate.code.sms.SmsCodeSender;
import com.imooc.security.core.validate.code.sms.impl.DefaultSmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 17:11
 */
@Configuration
public class ValidateCodeBeanConfig {
    @Autowired
    private SecurityProperties securityProperties;

    private static Properties captchaProperties = new Properties();
    private static Config captchaConfig = new Config(captchaProperties);

    @Bean(name = "captchaProducer")
    public DefaultKaptcha captchaProducer() {
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        KaptchaProperties kaptchaProperties = securityProperties.getCode().getKaptcha();
        //是否有边框  默认为true  我们可以自己设置yes，no
        captchaProperties.setProperty(Constants.KAPTCHA_BORDER, kaptchaProperties.getBorder());
        //边框颜色   默认为Color.BLACK
        captchaProperties.setProperty(Constants.KAPTCHA_BORDER_COLOR, kaptchaProperties.getBorderColor());
        //边框粗细度  默认为1
        captchaProperties.setProperty(Constants.KAPTCHA_BORDER_THICKNESS, kaptchaProperties.getBorderThickness());
        //验证码图片宽度  默认为200
        captchaProperties.setProperty(Constants.KAPTCHA_IMAGE_WIDTH, kaptchaProperties.getImageWidth());
        // 验证码图片高度  默认为50
        captchaProperties.setProperty(Constants.KAPTCHA_IMAGE_HEIGHT, kaptchaProperties.getImageHeight());
        //图片实现类  默认为	com.google.code.kaptcha.impl.DefaultKaptcha
        captchaProperties.setProperty(Constants.KAPTCHA_PRODUCER_IMPL, kaptchaProperties.getProducerImpl());
        //验证码文本生成器  默认为    com.google.code.kaptcha.text.impl.DefaultTextCreator
        captchaProperties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_IMPL, kaptchaProperties.getTextProducerImpl());
        //验证码文本字符颜色  默认为Color.BLACK
        captchaProperties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_COLOR, kaptchaProperties.getTextProducerFontColor());
        //验证码文本字符内容范围  默认为abcde2345678gfynmnpwx
        captchaProperties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING, kaptchaProperties.getTextProducerCharString());
        //验证码文本字符长度  默认为5
        captchaProperties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, kaptchaProperties.getTextProducerCharLength());
        //验证码文本字体样式  默认为new Font("Arial", 1, fontSize), new Font("Courier", 1, fontSize)
        captchaProperties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_NAMES, kaptchaProperties.getTextProducerFontNames());
        //验证码文本字符大小  默认为40
        captchaProperties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_SIZE, kaptchaProperties.getTextProducerFontSize());
        //验证码文本字符间距  默认为2
        captchaProperties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_SPACE, kaptchaProperties.getTextProducerCharSpace());
        //验证码噪点生成对象  默认为    com.google.code.kaptcha.impl.DefaultNoise
        captchaProperties.setProperty(Constants.KAPTCHA_NOISE_IMPL, kaptchaProperties.getNoiseImpl());
        //干扰颜色，合法值： r,g,b 或者 white,black,blue. 默认 black
        captchaProperties.setProperty(Constants.KAPTCHA_NOISE_COLOR, kaptchaProperties.getNoiseColor());
        //验证码样式引擎
//        水纹     com.google.code.kaptcha.impl.WaterRipple
//        鱼眼      com.google.code.kaptcha.impl.FishEyeGimpy
//        阴影      com.google.code.kaptcha.impl.ShadowGimpy
//        默认为    com.google.code.kaptcha.impl.WaterRipple
        captchaProperties.setProperty(Constants.KAPTCHA_OBSCURIFICATOR_IMPL, kaptchaProperties.getObscurificatorImpl());
        //验证码背景生成器    默认为    com.google.code.kaptcha.impl.DefaultBackground
        captchaProperties.setProperty(Constants.KAPTCHA_BACKGROUND_IMPL, kaptchaProperties.getBackgroundImpl());
        //验证码背景颜色渐进 开始   默认为Color.LIGHT_GRAY
        captchaProperties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_FROM, kaptchaProperties.getBackgroundClearFrom());
        //验证码背景颜色渐进 结束   默认为Color.
        captchaProperties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_TO, kaptchaProperties.getBackgroundClearTo());
        captchaProperties.setProperty(Constants.KAPTCHA_WORDRENDERER_IMPL, kaptchaProperties.getWordImpl());
        //默认 KAPTCHA_SESSION_KEY
        captchaProperties.setProperty(Constants.KAPTCHA_SESSION_CONFIG_KEY, kaptchaProperties.getSessionKey());
        //默认 KAPTCHA_SESSION_DATE
        captchaProperties.setProperty(Constants.KAPTCHA_SESSION_CONFIG_DATE, kaptchaProperties.getSessionDate());
        defaultKaptcha.setConfig(captchaConfig);
        return defaultKaptcha;
    }


    @Bean
    @ConditionalOnMissingBean(name = "imageCodeGenerator")
    public ImageCodeGenerator imageCodeGenerator() {
        ImageCodeGenerator imageCodeGenerator = new ImageCodeGenerator();
        imageCodeGenerator.setSecurityProperties(securityProperties);
        imageCodeGenerator.setCaptchaProducer(captchaProducer());
        imageCodeGenerator.setConfig(captchaConfig);
        imageCodeGenerator.setProperties(captchaProperties);
        return imageCodeGenerator;
    }

    @Bean
    @ConditionalOnMissingBean(name = "smsCodeGenerator")
    public SmsCodeGenerator smsCodeGenerator() {
        SmsCodeGenerator smsCodeGenerator = new SmsCodeGenerator();
        smsCodeGenerator.setSecurityProperties(securityProperties);
        smsCodeGenerator.setCaptchaProducer(captchaProducer());
        smsCodeGenerator.setConfig(captchaConfig);
        smsCodeGenerator.setProperties(captchaProperties);
        return smsCodeGenerator;
    }

    @Bean
    @ConditionalOnMissingBean(SmsCodeSender.class)
    public SmsCodeSender smsCodeSender() {
        return new DefaultSmsCodeSender();
    }

}
