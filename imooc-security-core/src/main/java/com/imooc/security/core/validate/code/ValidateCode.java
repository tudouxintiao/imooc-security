/**
 * 
 */
package com.imooc.security.core.validate.code;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @author zhailiang
 *
 */
@Setter
@Getter
public class ValidateCode implements Serializable {

	
	private String code;
	
	private LocalDateTime expireTime;
	
	public ValidateCode(String code, int expireIn){
		this.code = code;
		this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
	}
	
	public ValidateCode(String code, LocalDateTime expireTime){
		this.code = code;
		this.expireTime = expireTime;
	}
	//判断验证码是否过期
	public Boolean isExpired(){
		return LocalDateTime.now().isAfter(expireTime);
	}
}
