package com.imooc.security.core.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/23 0023
 * Time: 23:33
 */
@Setter
@Getter
public class QQProperties {

    private String appId;
    private String appSecret;

    private String providerId = "qq";

}