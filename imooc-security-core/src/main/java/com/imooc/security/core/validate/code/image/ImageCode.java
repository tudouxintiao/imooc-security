package com.imooc.security.core.validate.code.image;

import com.imooc.security.core.validate.code.ValidateCode;
import lombok.Getter;
import lombok.Setter;

import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/20 0020
 * Time: 15:04
 */
@Getter
@Setter
public class ImageCode extends ValidateCode {
  private BufferedImage image;

  public ImageCode(BufferedImage image, String code, int expireIn){
    super(code, expireIn);
    this.image = image;
  }

}
