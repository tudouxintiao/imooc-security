package com.imooc.security.core.validate.code.sms;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 18:19
 */

public interface SmsCodeSender {
    void send(String mobile,String code);
}
