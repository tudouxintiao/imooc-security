package com.imooc.security.core.social.qq.connect;

import com.imooc.security.core.social.qq.api.QQ;
import com.imooc.security.core.social.qq.api.QQImpl;
import com.imooc.security.core.social.qq.connect.QQOAuth2Template;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Template;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/23 0023
 * Time: 22:04
 */

public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQ> {

    private String appId;

    private static final String URL_AUTHORIZE ="https://graph.qq.com/oauth2.0/authorize";

    private static final String URL_ACCESS_TOKEN ="https://graph.qq.com/oauth2.0/token";

    @Override
    public QQ getApi(String accessToken) {
        return new QQImpl(accessToken,appId);
    }

    public QQServiceProvider(String appId,String appSecret) {
        //自定义QQOauth2Template解决qq返回contentType为text/html格式的问题
        super(new QQOAuth2Template(appId,appSecret,URL_AUTHORIZE,URL_ACCESS_TOKEN));
        this.appId =appId;
    }
}
