package com.imooc.security.core.validate.code;

import org.springframework.security.core.AuthenticationException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 10:59
 */

public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String msg) {
        super(msg);
    }
}
