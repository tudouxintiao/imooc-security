package com.imooc.security.core.properties;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/15 0015
 * Time: 0:19
 */

public enum LoginType {

    REDIRECT,
    JSON
}
