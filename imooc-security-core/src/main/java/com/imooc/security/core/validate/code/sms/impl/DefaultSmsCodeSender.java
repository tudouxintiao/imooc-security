package com.imooc.security.core.validate.code.sms.impl;

import com.imooc.security.core.validate.code.sms.SmsCodeSender;
import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 18:20
 */
@Getter
@Setter
public class DefaultSmsCodeSender implements SmsCodeSender {

    @Override
    public void send(String mobile, String code) {
        System.out.println("向手机"+mobile+"发送短信验证码: " +code);
    }

}
