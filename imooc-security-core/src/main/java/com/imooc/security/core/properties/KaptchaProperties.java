package com.imooc.security.core.properties;

import com.imooc.security.core.validate.code.CustomNoise;
import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 19:21
 */
@Setter
@Getter
public class KaptchaProperties {

    //#kaptcha验证码详细配置
//    border=yes
//    border-color=black
//    border-thickness=1
//    image-width=200
//    image-height=50
//    producer-impl=com.google.code.kaptcha.impl.DefaultKaptcha
//    text-producer-impl=com.google.code.kaptcha.text.impl.DefaultTextCreator
//    text-producer-char-string=abcde2345678gfynmnpwx
//    text-producer-char-length=5
//    text-producer-font-names=Arial, Courier
//    text-producer-font-size=40
//    text-producer-font-color=black
//    text-producer-char-space=2
//    noise-impl=com.google.code.kaptcha.impl.DefaultNoise
//    noise-color=black
//    obscurificator-impl=com.google.code.kaptcha.impl.WaterRipple
//    background-impl=com.google.code.kaptcha.impl.DefaultBackground
//    background-clear-from=192,192,192
//    background-clear-to=white
//    word-impl=com.google.code.kaptcha.text.impl.DefaultWordRenderer
//    session-key=KAPTCHA_SESSION_KEY
//    session-date=KAPTCHA_SESSION_DATE
    private String border = "yes";
    private String borderColor = "black";
    private String borderThickness = "1";
    private String imageWidth = "200";
    private String imageHeight = "50";
    private String producerImpl = "com.google.code.kaptcha.impl.DefaultKaptcha";
    private String textProducerImpl = "com.google.code.kaptcha.text.impl.DefaultTextCreator";
    private String textProducerCharString = "1234567890";
    private String textProducerCharLength = "6";
    private String textProducerFontNames = "Arial,Times New Roman";
    private String textProducerFontSize = "40";
    private String textProducerFontColor = "black";
    private String textProducerCharSpace = "4";
    private String noiseImpl = "com.imooc.security.core.validate.code.CustomNoise";
    private String noiseColor = "red";
    private String obscurificatorImpl = "com.google.code.kaptcha.impl.WaterRipple";
    private String backgroundImpl = "com.google.code.kaptcha.impl.DefaultBackground";
    private String backgroundClearFrom = "192,192,192";
    private String backgroundClearTo = "white";
    private String wordImpl = "com.google.code.kaptcha.text.impl.DefaultWordRenderer";
    private String sessionKey = "KAPTCHA_SESSION_KEY";
    private String sessionDate = "KAPTCHA_SESSION_DATE";

}
