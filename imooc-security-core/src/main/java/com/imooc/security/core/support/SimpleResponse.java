package com.imooc.security.core.support;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/14 0014
 * Time: 21:51
 */

public class SimpleResponse {

    public SimpleResponse(Object content){
        this.content =content;
    }

    private Object content;

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
