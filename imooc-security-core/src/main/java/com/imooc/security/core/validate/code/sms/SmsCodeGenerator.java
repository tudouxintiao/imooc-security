package com.imooc.security.core.validate.code.sms;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import com.imooc.security.core.properties.SecurityProperties;
import com.imooc.security.core.validate.code.ValidateCodeGenerator;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.ServletRequest;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 17:00
 */
public class SmsCodeGenerator implements ValidateCodeGenerator {

    @Setter
    @Getter
    private DefaultKaptcha captchaProducer;

    @Setter
    @Getter
    private SecurityProperties securityProperties;

    @Getter
    @Setter
    private Config config;

    @Getter
    @Setter
    private Properties properties;

    @Override
    public SmsCode generate(ServletWebRequest request) {
        int length = ServletRequestUtils.getIntParameter(request.getRequest(),"length",securityProperties.getCode().getSms().getLength());
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH,String.valueOf(length));
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING,String.valueOf(securityProperties.getCode().getSms().getTextProducerCharString()));
        captchaProducer.setConfig(config);
        String code = captchaProducer.createText();
        return new SmsCode(code,securityProperties.getCode().getSms().getExpireIn());
    }
}
