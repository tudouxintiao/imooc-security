package com.imooc.security.core.validate.code;

import com.google.code.kaptcha.NoiseProducer;
import com.google.code.kaptcha.util.Configurable;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/8/2 0002
 * Time: 20:12
 */
@Component
public class CustomNoise  extends Configurable implements NoiseProducer {
    public CustomNoise() {
    }

    @Override
    public void makeNoise(BufferedImage image, float factorOne, float factorTwo, float factorThree, float factorFour) {
        Color color = this.getConfig().getNoiseColor();
        int width = image.getWidth();
        int height = image.getHeight();
        Point2D[] pts = null;
        Random rand = new Random();
        CubicCurve2D cc = new CubicCurve2D.Float((float)width * factorOne, (float)height * rand.nextFloat(), (float)width * factorTwo, (float)height * rand.nextFloat(), (float)width * factorThree, (float)height * rand.nextFloat(), (float)width * factorFour, (float)height * rand.nextFloat());
        PathIterator pi = cc.getPathIterator((AffineTransform)null, 2.0D);
        Point2D[] tmp = new Point2D[200];
        int i = 0;

        while(!pi.isDone()) {
            float[] coords = new float[6];
            switch(pi.currentSegment(coords)) {
                case 0:
                case 1:
                    tmp[i] = new java.awt.geom.Point2D.Float(coords[0], coords[1]);
                default:
                    ++i;
                    pi.next();
            }
        }

        pts = new Point2D[i];
        System.arraycopy(tmp, 0, pts, 0, i);
        Graphics2D graph = (Graphics2D)image.getGraphics();
        graph.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
        for(i = 0; i < width*height/200; ++i) {
            if (i < 3) {
                graph.setColor(color);
                graph.setStroke(new BasicStroke(0.9F * (float)(3 - i)));
            }
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);
            int xl = rand.nextInt(30);
            int yl = rand.nextInt(30);
            graph.setColor(getRandColor(40, 160));
            graph.drawLine(x, y, x + xl, y + yl);
        }

        graph.dispose();
    }

    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255) {
            fc = 255;
        }
        if (bc > 255) {
            bc = 255;
        }
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }
}