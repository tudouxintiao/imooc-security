package com.imooc.security.core.validate.code.sms;

import com.imooc.security.core.validate.code.ValidateCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/20 0020
 * Time: 15:04
 */
@Getter
@Setter
public class SmsCode extends ValidateCode {

  public SmsCode(String code, int expireIn){
    super(code, expireIn);
  }

}
