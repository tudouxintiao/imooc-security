package com.imooc.security.core.social.qq.api;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/23 0023
 * Time: 21:19
 */

public interface QQ {

    QQUserInfo getUserInfo();

}
