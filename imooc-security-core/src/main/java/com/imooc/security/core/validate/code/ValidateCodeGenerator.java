package com.imooc.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 16:51
 */

public interface ValidateCodeGenerator {
  ValidateCode generate(ServletWebRequest request);
}
