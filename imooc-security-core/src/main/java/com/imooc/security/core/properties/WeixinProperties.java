package com.imooc.security.core.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/23 0023
 * Time: 23:33
 */
@Setter
@Getter
public class WeixinProperties extends  QQProperties{

    //第三方id,用来决定第三方登录的url 默认是weixin
    public WeixinProperties() {
        setProviderId("weixin");
    }
}