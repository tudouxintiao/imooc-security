package com.imooc.security.core.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 13:31
 */
@Getter
@Setter
public class ValidateCodeProperties {
    private KaptchaProperties kaptcha = new KaptchaProperties();
    private ImageCodeProperties image = new ImageCodeProperties();
    private SmsCodeProperties sms = new SmsCodeProperties();
}