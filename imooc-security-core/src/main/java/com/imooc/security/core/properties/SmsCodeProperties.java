package com.imooc.security.core.properties;


import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/20 0020
 * Time: 20:25
 */
@Setter
@Getter
public class SmsCodeProperties {

    //过期时间,默认60s
    private Integer expireIn = 60;
    private String url;
    private int length = 6;
    private String textProducerCharString="1234567890";

}
