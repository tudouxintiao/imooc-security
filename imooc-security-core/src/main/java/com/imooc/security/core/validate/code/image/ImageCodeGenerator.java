package com.imooc.security.core.validate.code.image;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import com.imooc.security.core.properties.SecurityProperties;
import com.imooc.security.core.validate.code.ValidateCodeGenerator;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.awt.image.BufferedImage;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/21 0021
 * Time: 17:00
 */
public class ImageCodeGenerator implements ValidateCodeGenerator {

    @Setter
    @Getter
    private DefaultKaptcha captchaProducer;

    @Setter
    @Getter
    private SecurityProperties securityProperties;

    @Getter
    @Setter
    private Config config;

    @Getter
    @Setter
    private Properties properties;

    @Override
    public ImageCode generate(ServletWebRequest request) {
        int width = ServletRequestUtils.getIntParameter(request.getRequest(),"width",securityProperties.getCode().getImage().getWidth());
        int height = ServletRequestUtils.getIntParameter(request.getRequest(),"height",securityProperties.getCode().getImage().getHeight());
        properties.setProperty(Constants.KAPTCHA_IMAGE_WIDTH,String.valueOf(width));
        properties.setProperty(Constants.KAPTCHA_IMAGE_HEIGHT,String.valueOf(height));
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH,String.valueOf(securityProperties.getCode().getImage().getLength()));
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING,String.valueOf(securityProperties.getCode().getImage().getTextProducerCharString()));
        // create the text
        captchaProducer.setConfig(config);
        String code = captchaProducer.createText();
        // create the image with the text
        BufferedImage image= captchaProducer.createImage(code);
        return new ImageCode(image,code,securityProperties.getCode().getImage().getExpireIn());
    }
}
