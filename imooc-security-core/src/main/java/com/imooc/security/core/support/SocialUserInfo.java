package com.imooc.security.core.support;

import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/30 0030
 * Time: 22:23
 */
@Setter
@Getter
public class SocialUserInfo {
    private String providerId;
    private String providerUserId;
    private String nickName;
    private String headImg;
}
