package com.imooc.security.core.properties;


import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/20 0020
 * Time: 20:25
 */
@Setter
@Getter
public class ImageCodeProperties extends SmsCodeProperties{

    public ImageCodeProperties() {
        setLength(4);
        setTextProducerCharString("1234567890");
    }

    private int width = 150;
    private int height = 50;

}
