package com.imooc.security.core.validate.code;

import com.google.code.kaptcha.Constants;
import com.imooc.security.core.properties.SecurityConstants;
import com.imooc.security.core.validate.code.image.ImageCode;
import com.imooc.security.core.validate.code.image.ImageCodeGenerator;
import com.imooc.security.core.validate.code.sms.SmsCode;
import com.imooc.security.core.validate.code.sms.SmsCodeGenerator;
import com.imooc.security.core.validate.code.sms.SmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/20 0020
 * Time: 19:57
 */
@RestController
public class ValidateCodeController {

//    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
//
//    @Autowired
//    private ImageCodeGenerator imageCodeGenerator;
//
//    @Autowired
//    private SmsCodeGenerator smsCodeGenerator;
//
//    @Autowired
//    private SmsCodeSender smsCodeSender;
//
//
//    /**
//     * 图形验证码
//     * @param request
//     * @param response
//     * @throws IOException
//     */
//    @GetMapping("/code/image")
//    public void createImageCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        response.setDateHeader("Expires", 0);// 禁止server端缓存
//        // 设置标准的 HTTP/1.1 no-cache headers.
//        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
//        // 设置IE扩展 HTTP/1.1 no-cache headers (use addHeader).
//        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
//        response.setHeader("Pragma", "no-cache");// 设置标准 HTTP/1.0 不缓存图片
//        response.setContentType("image/jpeg");// 返回一个 jpeg 图片，默认是text/html(输出文档的MIMI类型)
//        ImageCode imageCode = imageCodeGenerator.generate(new ServletWebRequest(request));
//        sessionStrategy.setAttribute(new ServletWebRequest(request), Constants.KAPTCHA_SESSION_KEY, imageCode);
//        ImageIO.write(imageCode.getImage(), "JPEG", response.getOutputStream());
//    }
//
//    /**
//     * 短信验证码
//     * @param request
//     * @param response
//     * @throws IOException
//     */
//    @GetMapping("/code/sms")
//    public void createSmsCode(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
//        SmsCode smsCode = smsCodeGenerator.generate(new ServletWebRequest(request));
//        sessionStrategy.setAttribute(new ServletWebRequest(request), Constants.KAPTCHA_SESSION_KEY, smsCode.getCode());
//        String mobile = ServletRequestUtils.getRequiredStringParameter(request,"mobile");
//        //短信供应商接口
//        smsCodeSender.send(mobile,smsCode.getCode());
//    }

    @Autowired
    private Map<String,ValidateCodeProcessor> validateCodeProcessors;


    /**
     * 根据验证码的类型调用对应的处理器
     * @param request
     * @param response
     * @param type
     * @throws Exception
     */
    @GetMapping("/code/{type}")
    public void createCode(HttpServletRequest request, HttpServletResponse response
    , @PathVariable String type) throws Exception{

        validateCodeProcessors.get(type+ SecurityConstants.PROCESSOR_SUFFIX).create(new ServletWebRequest(request, response));


    }





}
