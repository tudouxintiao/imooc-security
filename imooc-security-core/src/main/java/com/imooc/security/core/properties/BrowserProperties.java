package com.imooc.security.core.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/14 0014
 * Time: 21:58
 */
@Setter
@Getter
public class BrowserProperties {
    private SessionProperties session = new SessionProperties();
    private String signUpUrl="/default-signUp.html";

    private String loginPage="/default-signIn.html";

    private String logoutUrl="/default-logout.html";

    private LoginType loginType = LoginType.JSON;

    private Integer rememberMeSeconds = 1080;//默认7天


}
