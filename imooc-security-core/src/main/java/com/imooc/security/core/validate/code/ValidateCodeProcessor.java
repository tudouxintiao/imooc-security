package com.imooc.security.core.validate.code;


import com.google.code.kaptcha.Constants;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 校验码处理器,封装不同校验码的处理逻辑
 */
public interface ValidateCodeProcessor {

    /**
     * 验证码放入session的前缀
     */
    String SEESION_KEY_PREFIX = Constants.KAPTCHA_SESSION_KEY+"_";

    /**
     * 创建验证码
     * @param request
     * @throws Exception
     */
    void create(ServletWebRequest request)throws  Exception;


    /**
     * 校验验证码
     *
     * @param servletWebRequest
     * @throws Exception
     */
    void validate(ServletWebRequest servletWebRequest);
}
