package com.imooc.security.app;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/8/5 0005
 * Time: 0:05
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig  {//extends ResourceServerConfigurerAdapter

}
