package com.imooc.validator;

import com.imooc.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/7 0007
 * Time: 21:09
 */

public class MyConstraintValidator implements ConstraintValidator<MyConstraint,Object>{

    @Autowired
    private HelloService helloService;

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        helloService.greeting("zhangsan");
        System.out.println(value);
        return true;
    }

    @Override
    public void initialize(MyConstraint constraintAnnotation) {
        System.out.println(" my validator init");
    }
}
