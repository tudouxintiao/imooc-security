package com.imooc.service.impl;

import com.imooc.service.HelloService;
import com.sun.org.apache.xerces.internal.impl.dv.dtd.NMTOKENDatatypeValidator;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/7 0007
 * Time: 21:17
 */
@Service
public class HelloServiceImpl  implements HelloService{

    @Override
    public String greeting(String name) {
        return "hello "+ name;
    }
}
