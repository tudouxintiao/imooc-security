package com.imooc.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/14 0014
 * Time: 12:54
 */
@Setter
@Getter
public class FileInfo {

    public FileInfo (String path){
        this.path =path;
    }
    private String path;
}
