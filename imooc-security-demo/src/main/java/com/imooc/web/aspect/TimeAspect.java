//package com.imooc.web.aspect;
//
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//
///**
// * Created with IntelliJ IDEA.
// * User: Administrator
// * Date: 2019/7/14 0014
// * Time: 12:10
// */
//@Aspect
//@Component
//public class TimeAspect {
//
//    @Around("execution(* com.imooc.web.controller.UserController.*(..))")
//    public Object handlerControllerMethod(ProceedingJoinPoint joinPoint) throws Throwable {
//        System.out.println("time aspect start");
//        Object[] args = joinPoint.getArgs();
//        for (Object arg : args) {
//            System.out.println("arg is " + arg);
//        }
//
//        long start = new Date().getTime();
//        Object object = joinPoint.proceed();
//        System.out.println("time aspect 耗时:" + (new Date().getTime() - start));
//        System.out.println("time aspect end");
//        return object;
//    }
//
//}
