package com.imooc.web.async;

import net.bytebuddy.asm.Advice;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/14 0014
 * Time: 14:09
 */
@Component
public class QueueListener implements ApplicationListener<ContextRefreshedEvent>{
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MockQueue mockQueue;

    @Autowired
    private DeferredResultHolder deferredResultHolder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

       new Thread(()->{
           while(true){
               if(StringUtils.isNotBlank(mockQueue.getComplateOrder())){
                   String orderNumber = mockQueue.getComplateOrder();
//                   logger.info("返回订单处理结果:"+orderNumber);
                   deferredResultHolder.getMap().get(orderNumber).setResult("place order success");
                   mockQueue.setComplateOrder(null);
               }else{
                   try {
                       Thread.sleep(1000);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }
           }
       }).start();

    }
}
