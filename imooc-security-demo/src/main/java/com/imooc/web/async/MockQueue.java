package com.imooc.web.async;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/14 0014
 * Time: 13:56
 */
@Component
public class MockQueue {

    private String placeOrder;

    private String complateOrder;

    public String getPlaceOrder() {
        return placeOrder;
    }

    public void setPlaceOrder(String placeOrder) {
       new Thread(()->{
//           System.out.println("接到下单请求"+placeOrder);
           try {
               Thread.sleep(1000);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
           this.complateOrder =placeOrder;
//           System.out.println("下单请求处理完毕,"+placeOrder);
       }).start();
    }

    public String getComplateOrder() {
        return complateOrder;
    }

    public void setComplateOrder(String complateOrder) {
        this.complateOrder = complateOrder;
    }
}
