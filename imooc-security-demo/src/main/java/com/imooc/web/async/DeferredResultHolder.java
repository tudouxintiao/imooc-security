package com.imooc.web.async;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import javax.swing.*;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/14 0014
 * Time: 14:00
 */
@Component
public class DeferredResultHolder {

    private Map<String,DeferredResult<String>> map=Maps.newHashMap();

    public Map<String, DeferredResult<String>> getMap() {
        return map;
    }

    public void setMap(Map<String, DeferredResult<String>> map) {
        this.map = map;
    }
}
