package com.imooc.web.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.imooc.dto.User;
import com.imooc.dto.UserQueryCondition;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/7 0007
 * Time: 14:17
 */
@RestController
public class UserController {

@Autowired
private ProviderSignInUtils providerSignInUtils;

    @PostMapping("/user/register")
    public void register(User user, HttpServletRequest request){
        //注册用户
        //不管是注册还是绑定,都会拿到用户的一个唯一标识存入数据库
        String userId = user.getUsername();//唯一的,可以自定义
        providerSignInUtils.doPostSignUp(userId,new ServletWebRequest(request));
    }

    @GetMapping("/me")
    public Object getCurrentUser(@AuthenticationPrincipal UserDetails userDetails){
        return userDetails;
    }


    @PostMapping("/user")
    @ApiOperation(value = "创建用户")
    @JsonView(User.UserSimpleView.class)
    public User create(@Valid @RequestBody User user, BindingResult errors){
        if(errors.hasErrors()){
            errors.getAllErrors().stream().forEach(
                    error-> System.out.println(error.getDefaultMessage())
            );
        }
        System.out.println(user);
        user.setId("1");
        return user;
    }


    @GetMapping("/user")
    @JsonView(User.UserSimpleView.class)
    @ApiOperation(value = "用户查询服务")
    public List<User> query(UserQueryCondition condition, @PageableDefault(page = 1,size = 10,sort ="username asc" ) Pageable pageable){
        System.out.println(condition);
        System.out.println(pageable.getPageSize());
        System.out.println(pageable.getPageNumber());
        System.out.println(pageable.getSort());
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());
        users.add(new User());
        return users;
    }

    @GetMapping(value = "/user/{id:\\d+}")
    @JsonView(User.UserDetailView.class)
    public User getInfo(@ApiParam(value = "用户ID") @PathVariable String id){
//        throw new UserNotExistException(id);
        System.out.println("getInfo .........");
        User user = new User();
        user.setUsername("tom");
        return user;
    }

    @PutMapping(value = "/user/{id:\\d+}")
    public User updateInfo(@Valid @RequestBody User user,BindingResult errors){
        if(errors.hasErrors()){
            errors.getAllErrors().stream().forEach(
                    error-> {
                        FieldError fieldError = (FieldError)error;
                        String message =fieldError.getField()+" "+error.getDefaultMessage();
                        System.out.println(message);
                    }
            );
        }
        System.out.println(user);
        return user;
    }

    @DeleteMapping("/user/{id:\\d+}")
    public void delete(@PathVariable String id){
        System.out.println(id);
    }

}
