package com.imooc.web.controller;

import com.imooc.exception.UserNotExistException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/13 0013
 * Time: 0:17
 */
@ControllerAdvice
public class ControllerExceptionHandler {

 @ExceptionHandler(UserNotExistException.class)
 @ResponseBody
 @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
 public Map<String,Object> handlerUserNotExistException(UserNotExistException exist){
     Map<String,Object> result = new HashMap<>();
     result.put("id", exist.getId());
     result.put("message",exist.getMessage() );
     return result;
 }


}
