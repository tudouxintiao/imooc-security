package com.imooc.exception;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/13 0013
 * Time: 0:11
 */

public class UserNotExistException extends  RuntimeException{

   private String id;

  public UserNotExistException(String id){

      super("user not exist!");
      this.id =id;
  }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
