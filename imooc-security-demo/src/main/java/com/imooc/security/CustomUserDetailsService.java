package com.imooc.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.security.SocialUser;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Component;


/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/14 0014
 * Time: 19:26
 */

@Component
public class CustomUserDetailsService implements UserDetailsService, SocialUserDetailsService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        logger.info("登录用户ID: "+userId);
        return getUserDetails(userId);
    }

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //根据用户名查询用户信息
        logger.info("登录用户名: "+username);
       return getUserDetails(username);
    }

    private SocialUserDetails getUserDetails(String username) {

//        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        //根据查找到的用户判断用户是否被冻结
        //这里的密码实际业务是注册的时候加密后放进数据库的密码,这里假定是数据库的密码,所以加密放入
        String password =passwordEncoder.encode("123456");
        //这里的密码实际业务是注册的时候加密后放进数据库的密码,这里假定是数据库的密码,所以加密放入
        return new SocialUser(username,password,
                true,true,true,true, AuthorityUtils.commaSeparatedStringToAuthorityList("admin,ROLE_USER"));
    }
}
