package com.imooc.web.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import javax.sound.midi.Soundbank;
import javax.validation.Valid;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/13 0013
 * Time: 19:19
 */

public class TestMain {


    /**
     * 将一个 JavaBean 对象转化为一个  Map
     *
     * @param bean 要转化的JavaBean 对象
     * @return 转化出来的  Map 对象
     * @throws IntrospectionException    如果分析类属性失败
     * @throws IllegalAccessException    如果实例化 JavaBean 失败
     * @throws InvocationTargetException 如果调用属性的 setter 方法失败
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static <T> Map convertBeanToMap(T bean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        Class type = bean.getClass();
        Map returnMap = Maps.newHashMap();
        BeanInfo beanInfo = Introspector.getBeanInfo(type);
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (int i = 0; i < propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (!propertyName.equals("class")) {
                Method readMethod = descriptor.getReadMethod();
                Object result = readMethod.invoke(bean, new Object[0]);
                returnMap.put(propertyName, result);
            }
        }
        return returnMap;
    }


    /**
     * 将一个 Map 对象转化为一个 JavaBean
     *
     * @param type 要转化的类型
     * @param map  包含属性值的 map
     * @return 转化出来的 JavaBean 对象
     * @throws IntrospectionException    如果分析类属性失败
     * @throws IllegalAccessException    如果实例化 JavaBean 失败
     * @throws InstantiationException    如果实例化 JavaBean 失败
     * @throws InvocationTargetException 如果调用属性的 setter 方法失败
     */
    @SuppressWarnings("rawtypes")
    public static <T> T convertMapToBean(Class<T> type, Map map)
            throws IntrospectionException, IllegalAccessException,
            InstantiationException, InvocationTargetException {
        BeanInfo beanInfo = Introspector.getBeanInfo(type); // 获取类属性
        T obj = type.newInstance(); // 创建 JavaBean 对象
        // 给 JavaBean 对象的属性赋值
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (int i = 0; i < propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (map.containsKey(propertyName)) {
                // 下面一句可以 try 起来，这样当一个属性赋值失败的时候就不会影响其他属性赋值。
                Object value = map.get(propertyName);
                Object[] args = new Object[1];
                args[0] = value;
                descriptor.getWriteMethod().invoke(obj, args);
            }
        }
        return type.cast(obj);
    }

    /**
     * 将驼峰式命名的字符串转换为下划线大写方式。如果转换前的驼峰式命名的字符串为空，则返回空字符串。
     * 例如： prefix = F_ , parentId -> F_PARENT_ID
     *
     * @param prefix    需要拼接的前缀
     * @param fieldName 转换前的驼峰式命名的字符串
     * @return 转换后下划线大写方式命名的字符串
     */
    public static String underscoreEntityToUpperCaseAndUnderLine(String prefix, String fieldName) {
        StringBuilder result = new StringBuilder();
        if (fieldName != null && fieldName.length() > 0) {
            if (StringUtils.isNotBlank(prefix)) {
                result.append(prefix);
            }
            // 将第一个字符处理成大写
            result.append(fieldName.substring(0, 1).toUpperCase());
            // 循环处理其余字符
            for (int i = 1; i < fieldName.length(); i++) {
                String s = fieldName.substring(i, i + 1);
                // 在大写字母前添加下划线
                if (s.equals(s.toUpperCase()) && !Character.isDigit(s.charAt(0))) {
                    result.append("_");
                }
                // 其他字符直接转成大写
                result.append(s.toUpperCase());
            }
        }
        return result.toString();
    }


    /**
     * 普通实体的业务内容交换,直接交换id
     * @param o1
     * @param o2
     * @param <T>
     * @return
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    public static <T> List<T> exchangeSimpleEntity(T o1, T o2) throws IntrospectionException, IllegalAccessException,
            InstantiationException, InvocationTargetException {
        Map<String, Object> oMap1 = convertBeanToMap(o1);
        Map<String, Object> oMap2 = convertBeanToMap(o2);
        List<T> mapList = Lists.newArrayList();
        Object idTempValue = oMap1.get("id");
        oMap1.put("id", oMap2.get("id"));
        oMap2.put("id", idTempValue);
        mapList.add(convertMapToBean((Class<T>) o1.getClass(), oMap1));
        mapList.add(convertMapToBean((Class<T>) o2.getClass(), oMap2));
        return mapList;
    }


    /**
     *   特殊实体的业务内容交换,先交换id
     *   在处理  唯一性code, levelId, parentId
     *   生成一条临时记录,一共三条数据 , 按照严格顺序放进 list
     * @param parent
     * @param children
     * @param <T>
     * @return
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    public static <T> List<T> exchangeSpecialEntity(T parent, T children) throws IntrospectionException, IllegalAccessException,
            InstantiationException, InvocationTargetException {
        Map<String, Object> parentMap = convertBeanToMap(parent);
        Map<String, Object> childrenMap = convertBeanToMap(children);
        List<T> mapList = Lists.newArrayList();
       //拿到parent的levelId前缀, 例如: "100000-100001" -> "100000-" 或者 1000001 -> ""
        String[] levelIds = String.valueOf(parentMap.get("levelId")).split(String.valueOf(parentMap.get("id")));
        String levelIdPrefix ="";
        if(levelIds.length>0){
            levelIdPrefix = levelIds[0];
        }
        //交换id,也就是业务内容交换
        Object childrenMapIdTempValue = childrenMap.get("id");
        childrenMap.put("id", parentMap.get("id"));
        parentMap.put("id", childrenMapIdTempValue);
        //更新级次串
        parentMap.put("levelId", levelIdPrefix + parentMap.get("id"));
        childrenMap.put("levelId", parentMap.get("levelId") + "-" + childrenMap.get("id"));
        //更新父级id
        childrenMap.put("parentId", parentMap.get("id"));
        //数据库中code为唯一性约束,这里先使用uuid构建一个临时记录,所以为三条记录,记录顺序不能改动
        Object childrenUniFieldValue = childrenMap.get("code");
        //临时code记录
        Map<String, Object> childrenMapTemp =Maps.newHashMap();
        childrenMapTemp.putAll(childrenMap);
        childrenMapTemp.put("code", childrenUniFieldValue + "_" + UUID.randomUUID().toString());
        //构建好交换记录数,加上原有的2条,一共三条,可以调用构建批量sql的方法更新进数据库
        mapList.add(convertMapToBean((Class<T>) children.getClass(), childrenMapTemp));
        mapList.add(convertMapToBean((Class<T>) parent.getClass(), parentMap));
        mapList.add(convertMapToBean((Class<T>) children.getClass(), childrenMap));
        return mapList;
    }

    /**
     * 通过实体构建数据库表字段映射
     * @param o
     * @param <T>
     * @return
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    public static <T> Map<String ,Object> convertEntityFieldMap(T o) throws IntrospectionException, IllegalAccessException,
             InvocationTargetException {
        Map<String,Object> entityMap = convertBeanToMap(o);
        Map<String,Object> fieldMap = Maps.newHashMap();
        entityMap.forEach((key, value)->{
            fieldMap.put(underscoreEntityToUpperCaseAndUnderLine("F_", key), value);
        });
        return fieldMap;
    }

    /**
     * 通过实体集合构建数据库表字段映射集合
     * @param oList
     * @param <T>
     * @return
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    public static <T> List<Map<String ,Object>> convertEntityFieldMapList(List<T> oList) throws IntrospectionException, IllegalAccessException,
            IllegalAccessException, InvocationTargetException {
        List<Map<String ,Object>> fieldMapList = Lists.newArrayList();
        for (T entity:oList){
            fieldMapList.add(convertEntityFieldMap(entity));
        }
        return fieldMapList;
    }


    public static void main(String[] args) throws Exception {
        TestEntity parent = new TestEntity();
        parent.setId("100001");
        parent.setCode("code001");
        parent.setParentId("100000");
        parent.setLevelId("99999-100000-100001");
        parent.setLevel(3);
        parent.setLeaf(false);


        TestEntity children = new TestEntity();
        children.setId("100002");
        children.setCode("code002");
        children.setParentId("100001");
        children.setLevelId("99999-100000-100001-100002");
        children.setLevel(4);
        children.setLeaf(true);

        System.out.println(parent);
        System.out.println(children);
        System.out.println("-----------------------------------");

        //        String[] strs = parent.getLevelId().split(parent.getId());
//        System.out.println(strs[0]);
//        String parentId = parent.getParentId();
//
//        String  childrenTempId  = children.getId();
//        children.setId(parent.getId());
//        parent.setId(childrenTempId);
//
//        parent.setLevelId(strs[0]+parent.getId());
//        children.setLevelId(parent.getLevelId()+"-"+children.getId());
//
//       children.setParentId(parent.getId());
//
//        System.out.println(parent);
//        System.out.println(children);

        convertEntityFieldMap(children).forEach((key,value)->{
            System.out.println(key + " = "+value);
        });

        List<TestEntity> entities = TestMain.exchangeSpecialEntity(parent,children);
        entities.stream().forEach(entitie->{
            System.out.println(entitie);
        });





    }



//    public static void main(String[] args) throws Exception{
//        TestEntity test= new TestEntity();
//        test.setId("111111");
//        test.setCode(null);
//        test.setParentId("100000");
//        test.setLevel(2);
//        test.setLevelId("99999-100000-111111");
//        test.setLeaf(false);
//        Map map = new HashMap();
//        map.put("id", "55555");
//        map.put("code","code111" );
//        map.put("parentId", null);
//        map.put("levelId","99999-100000-111111" );
//        map.put("level", 2);
//        map.put("leaf", false);
//        TestEntity o  = TestMain.convertMapToBean(test.getClass(), map);
//        System.out.println(o.getParentId() == null);
//
//        Map<String ,Object> map1 = TestMain.convertBeanToMap(test);
//
//        map1.forEach((key,value)->{
//            System.out.println(key + " = "+value);
//        });
//
//    }


}
