package com.imooc.web.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/7/13 0013
 * Time: 19:17
 */

public class TestEntity {
    private String id;
    private String code;
    private String parentId;
    private String levelId;
    private Integer level;
    private Boolean leaf;

    @Override
    public String toString() {
        return "TestEntity{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", parentId='" + parentId + '\'' +
                ", levelId='" + levelId + '\'' +
                ", level=" + level +
                ", leaf=" + leaf +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

}
